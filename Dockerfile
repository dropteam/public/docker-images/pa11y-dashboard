FROM node:13

ENV NODE_ENV=production

RUN apt-get update
RUN apt-get install -y libnss3 libgconf-2-4 wget gnupg \
    && wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - \
    && sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list' \
    && apt-get update \
    && apt-get install -y google-chrome-unstable fonts-ipafont-gothic fonts-wqy-zenhei fonts-thai-tlwg fonts-kacst fonts-freefont-ttf \
      --no-install-recommends \
    && rm -rf /var/lib/apt/lists/*

RUN npm i puppeteer \
    # Add user so we don't need --no-sandbox.
    # same layer as npm install to keep re-chowned files from using up several hundred MBs more space
    && groupadd -r pptruser && useradd -r -g pptruser -G audio,video pptruser \
    && mkdir -p /home/pptruser/Downloads \
    && chown -R pptruser:pptruser /home/pptruser \
    && chown -R pptruser:pptruser /node_modules

RUN curl https://codeload.github.com/pa11y/pa11y-dashboard/tar.gz/3.1.0 --output pa11y-dashboard.tar.gz && \
    tar -zxvf pa11y-dashboard.tar.gz

RUN mv pa11y-dashboard-3.1.0 /app

## Add config file to your custom Dockerfile
#ADD ./config/config.json /app/config/production.json

WORKDIR /app
RUN chown -R pptruser:pptruser /app

USER pptruser
RUN npm install

CMD node index.js
#ENTRYPOINT ["tail", "-f", "/dev/null"]

